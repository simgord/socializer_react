import React, { Component } from 'react';
import { BrowserRouter as Link, NavLink } from 'react-router-dom';
import logo from '../assets/img/logo_socializer.svg';

class HeaderLogin extends Component {
  render() {
    return (
      <header className='header'>
        <div className='header-logo'>
          <NavLink to='/' className='nav-link'>
            <img src={logo} alt='okok' />
          </NavLink>
        </div>
        <div className='login-section'>
          Vous n’avez pas encore de compte ?
          <NavLink
            to='/signup'
            className='btn btn-rounded btn-transparent btn-border--blue'
          >
            inscription
          </NavLink>
        </div>
      </header>
    );
  }
}

export default HeaderLogin;
