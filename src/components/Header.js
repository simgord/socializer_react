import React, { Component } from 'react';
import { BrowserRouter, Link, NavLink } from 'react-router-dom';
import logo from '../assets/img/logo_socializer.svg';

class Header extends Component {
  render() {
    return (
      <header className='header'>
        <div className='header-logo'>
          <NavLink to='/' className='nav-link'>
            <img src={logo} alt='logo' />
          </NavLink>
        </div>

        <div className='header-menu'>
          <nav className='menu-wrapper'>
            <ul className='nav'>
              <li className='nav-item'>
                <Link to='/' className='nav-link'>
                  Why Socialize
                </Link>
              </li>
              <li className='nav-item'>
                <Link to='/' className='nav-link'>
                  Features
                </Link>
              </li>
              <li className='nav-item'>
                <Link to='/pricing' className='nav-link'>
                  Pricing
                </Link>
              </li>
              <li className='nav-item'>
                <NavLink
                  to='/signup'
                  className='nav-link btn btn-rounded btn-transparent btn-border--blue'
                >
                  Register
                </NavLink>
              </li>
            </ul>

            <div className='social-wrapper'>
              <ul>
                <li>
                  <a href='//facebook.com' className='nav-item'>
                    <i className='fab fa-facebook-f' />
                  </a>
                  <a href='//instagram.com/lwlz.inc/' className='nav-item'>
                    <i className='fab fa-instagram' />
                  </a>
                  <a
                    href='//linkedin.com/in/anis-barka-152083152/'
                    className='nav-item'
                  >
                    <i className='fab fa-linkedin-in' />
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}
export default Header;
