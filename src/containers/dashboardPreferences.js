import React, { Component } from 'react';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';

export default class dashboardPreferences extends Component {
  state = {
    status: false
  };
  componentWillMount() {
    console.log('IM UP');
    window.fbAsyncInit = () => {
      window.FB.init({
        appId: '577189906130896',
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });

      window.FB.getLoginStatus(response => {
        console.log('IM CHECKING STATUS');
        this.statusChangeCallback(response);
        console.log(response);
      });
    };

    ((d, s, id) => {
      let js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
    console.log('the state', this.state);
  }

  statusChangeCallback(response) {
    if (response.status === 'connected') {
      console.log('Logged in and authenticated');
      this.setState({ status: true });
    } else {
      console.log('Not authenticated');
      this.setState({ status: false });
    }
  }

  loginFb = () => {
    console.log('CONNECTED');
    // window.FB.login(function(response) {
    //   if (response.authResponse) {
    //     console.log('Welcome!  Fetching your information.... ', response);
    //     window.FB.api('/me', function(response) {
    //       console.log('Good to see you, ' + response.name + '.');
    //     });
    //   } else {
    //     console.log('User cancelled login or did not fully authorize.');
    //   }
    // });
  };

  logoutFb() {
    console.log('LOGGGOUT');
    // window.FB.logout(function(response) {
    //   // Person is now logged out
    //   console.log('dissconnected successfully ');
    // });
  }

  render() {
    return (
      <main>
        <div className='dashboard-container'>
          <div id='mainContent'>
            <div className='title-dashboard'>
              <h2>Mes préférences</h2>
            </div>

            <div className='py-5'>
              <div className='py-3'>
                {this.state.status ? (
                  <button
                    onClick={this.logoutFb}
                    type='button'
                    className='btn btn-squared btn-transparent btn-border--blue'
                    data-toggle='modal'
                    data-target='#modalPhoto'
                  >
                    <i className='fab fa-facebook-f' />
                    Déconnecter facebook
                  </button>
                ) : (
                  <button
                    onClick={this.loginFb}
                    type='button'
                    className='btn btn-squared btn-transparent btn-border--blue'
                    data-toggle='modal'
                    data-target='#modalPhoto'
                  >
                    <i className='fab fa-facebook-f' />
                    Connecter facebook
                  </button>
                )}
              </div>

              <div className='py-3'>
                <button
                  type='button'
                  className='btn btn-squared btn-transparent btn-border--blue'
                  data-toggle='modal'
                  data-target='#modalPhoto'
                >
                  <i className='fab fa-instagram' />
                  Connecter Instagram
                </button>
              </div>
            </div>

            <div className='py-3'>
              <div className='title-dashboard'>
                <h2>Mon offre</h2>
              </div>

              <div className='py-5 home-section--features'>
                <div className='features-wrapper'>
                  <div className='feature-card'>
                    <h4>Offre start</h4>
                    <p>One account connection</p>
                    <span>300.00 €</span>
                    <div className='text-center mt-2'>
                      <button
                        onClick={this.logoutFb}
                        type='button'
                        className='btn btn-squared btn-transparent btn-border--blue'
                        data-toggle='modal'
                        data-target='#modalPhoto'
                      >
                        Commander
                      </button>
                    </div>
                  </div>

                  <div className='feature-card'>
                    <h4>Offre Premium</h4>
                    <p>Full options</p>
                    <span>1 000.00 €</span>
                    <div className='text-center mt-2'>
                      <button
                        type='button'
                        className='btn btn-squared btn-transparent btn-border--blue'
                        data-toggle='modal'
                        data-target='#modalPhoto'
                      >
                        Commander
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}
