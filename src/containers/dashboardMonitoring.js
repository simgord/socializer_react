import React, { Component } from 'react';
import { BrowserRouter as Link, NavLink } from 'react-router-dom';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';

export default class dashboardMonitoring extends Component {
  state = {
    data: {},
    friends: '',
    posts: [],
    status: ''
  };

  componentDidMount() {
    window.fbAsyncInit = () => {
      window.FB.init({
        appId: '577189906130896',
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });

      window.FB.getLoginStatus(response => {
        console.log('the go');
        this.statusChangeCallback(response);
      });
    };

    ((d, s, id) => {
      let js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
    console.log('the state', this.state);
  }

  fetchData() {
    window.FB.api(
      'https://graph.facebook.com//me?fields=id,name,birthday,friends,email,posts',
      response => {
        if (response && !response.error) {
          console.log('the respons', response);
          this.setState({ data: response });
          this.setState({ friends: response.friends.summary.total_count });
          this.setState({ posts: response.posts.data });
        }
      }
    );
  }

  statusChangeCallback(response) {
    if (response.status === 'connected') {
      console.log('Logged in and authenticated');
      this.fetchData();
    } else {
      console.log('Not authenticated');
    }
  }

  checkLoginState() {
    window.FB.getLoginStatus(response => {
      this.statusChangeCallback(response);
    });
  }

  render() {
    return (
      <main>
        <div id='mainContent'>
          <div className='title-dashboard'>
            <h2>Mes monitoring</h2>
          </div>

          <div className='py-5 home-section--features'>
            <h3 className='pb-3'>Facebook</h3>

            <a
              href='//facebook.com'
              className='mb-3 btn btn-squared btn-transparent btn-border--blue'
            >
              <i className='fab fa-facebook-f' />
              voir la page
            </a>

            <div className='features-wrapper'>
              <div className='feature-card'>
                <h4>{this.state.friends}</h4>
                <span>Number of friends</span>
              </div>

              <div className='feature-card'>
                <h4>{this.state.posts.length}</h4>
                <span>Number of posts</span>
              </div>

              <div className='feature-card'>
                <h4>1 500</h4>
                <span>Number of posts</span>
              </div>
            </div>
          </div>

          <div className='py-5 home-section--features'>
            <h3 className='pb-3'>Instagram</h3>

            <a
              href='//instagram.com'
              className='mb-3 btn btn-squared btn-transparent btn-border--blue'
            >
              <i className='fab fa-instagram' />
              voir la page
            </a>

            <div className='features-wrapper'>
              <div className='feature-card'>
                <h4>300</h4>
                <span>Number of followers</span>
              </div>

              <div className='feature-card'>
                <h4>1 500</h4>
                <span>Number of posts</span>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}
